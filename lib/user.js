/**
 * Module dependencies.
 */
var passport = require('passport');
var db = require('../db');

exports.info = [
  passport.authenticate('bearer', { session: false }),
  function(req, res) {
    // req.authInfo is set using the `info` argument supplied by
    // `BearerStrategy`.  It is typically used to indicate scope of the token,
    // and used in access control checks.  For illustrative purposes, this
    // example simply returns the scope in the response.
    res.json({ user_id: req.user.id, name: req.user.name, scope: req.authInfo.scope })
  }
]


exports.login = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err || !user) {
      return res.json({ success : false, message : 'authentication failed' });
    }
    req.login(user, function(err){
      if(err){
        return next(err);
      }
      return res.send({ success : true, user_id: user.id, message : 'authentication succeeded' });
    });
  })(req, res, next);
};

exports.logout = function(req, res) {
  req.logout();
  res.json({ success: true });
}

exports.account = function(req, res) {
  if(!req.user) {
    return res.status(401).json({ success: false});
  }
  return res.json({ user: req.user });
}


exports.register = function(req, res) {
  console.log('register');
  new db.User({
    username: req.body.username,
    password: req.body.password,
    email: req.body.email,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    status: '',
    role: 'admin', // FIXME role
    created: new Date(),
    updated: new Date(),
    token: ''
  }).save().then(function(user){
    console.log('saved');
    return res.json({ success: true, user_id: user.id });
  }).catch(function(err) {
    console.log('failed');
    return res.json({ success: false, message: err.message });
  });
}
