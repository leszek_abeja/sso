/**
 * Module dependencies.
 */
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var db = require('../db');

/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */
passport.use(new LocalStrategy(
  function(username, password, done) {
    db.User
      .where({username: username, password: password})
      .fetch()
      .then(function(user){
        if(!user) {
          return done(null, false);
        }
        return done(null, user.toJSON());
    }).catch(function(err){
      return done('not ok');
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  db.User
    .where({id: id})
    .fetch()
    .then(function(user){
      if (!user) {
        return done(null, false);
      }
      return done(null, user.toJSON());
  }).catch(done);
});

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.  Use of the client password strategy
 * allows clients to send the same credentials in the request body (as opposed
 * to the `Authorization` header).  While this approach is not recommended by
 * the specification, in practice it is quite common.
 */
passport.use(new BasicStrategy(
  function(username, password, done) {
    new db.Client({client_id: username, client_secret: password})
      .fetch()
      .then(function(client){
        if (!client) return done(null, false);
        return done(null, client.toJSON());
    }).catch(done);
  }
));

passport.use(new ClientPasswordStrategy(
  function(clientId, clientSecret, done) {
    new db.Client({client_id: clientId, client_secret: clientSecret})
      .fetch()
      .then(function(client){
        if (!client) return done(null, false);
        return done(null, client.toJSON());
    }).catch(done);
  }
));

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate either users or clients based on an access token
 * (aka a bearer token).  If a user, they must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
passport.use(new BearerStrategy(
  function(accessToken, done) {
    new db.Token({token: accessToken})
      .fetch()
      .then(function(token) {
        if (!token) { return done(null, false); }
        token = token.toJSON();
        if (token.user_id != null) {
          return new db.User({id: token.user_id})
            .fetch()
            .then(function(user) {
              if (!user) {
                return done(null, false);
              }
              // to keep this example simple, restricted scopes are not implemented,
              // and this is just for illustrative purposes
              var info = { scope: '*' }
              done(null, user.toJSON(), info);
          })
        } else {
          return new db.Client({id: token.client_id})
            .fetch()
            .then(function(client) {
              if(!client) {
                return done(null, false);
              }
              // to keep this example simple, restricted scopes are not implemented,
              // and this is just for illustrative purposes
              var info = { scope: '*' }
              done(null, client.toJSON(), info);
          });
        }
    }).catch(done);
  }
));
