/**
 * Module dependencies.
 */
var express = require('express');
var passport = require('passport');
var oauth2 = require('./lib/oauth2');
var user = require('./lib/user');
var client = require('./lib/client');
var util = require('util');

// Express configuration

var app = express.createServer(3000);
app.set('view engine', 'ejs');
app.set('layout', 'layout');
app.use(express.logger());
app.use(express.cookieParser());
app.use(express.bodyParser());
app.use(express.session({ secret: 'keyboard cat' }));

//app.use(function(req, res, next) {
//  console.log('-- session --');
//  console.dir(req.session);
//  console.log(util.inspect(req.session, true, 3));
//  console.log('-------------');
//  next()
//});

app.use(passport.initialize());
app.use(passport.session());
app.use(app.router);
app.use(express.errorHandler({ dumpExceptions: false, showStack: true }));

// Passport configuration

require('./lib/auth');

app.get('/', function(req, res){
  res.json({ message: 'Abeja OAuth2 service' });
});

app.get('/dialog/authorize', oauth2.authorization);
app.post('/dialog/authorize/decision', oauth2.decision);
app.post('/oauth/token', oauth2.token);

app.post('/api/login', user.login);
app.post('/api/logout', user.login);
app.get('/api/account', user.account);
app.post('/api/register', user.register);
app.get('/api/userinfo', user.info);
app.get('/api/clientinfo', client.info);

app.listen(3000);

module.exports = app;
