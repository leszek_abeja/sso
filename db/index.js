
var pg = require('pg');
var config = require('../config/local');

//var db = new pg.Client(config.connstring);

var knex = require('knex')(config.knex);

var bookshelf = require('bookshelf')(knex);

var User = bookshelf.Model.extend({tableName: 'users'});
var Token = bookshelf.Model.extend({tableName: 'tokens'});
var Client = bookshelf.Model.extend({tableName: 'clients'});
var AuthorizationCode = bookshelf.Model.extend({tableName: 'authcodes'});

exports.User = User;
exports.Client = Client;
exports.Token = Token;
