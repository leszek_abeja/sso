var db = require('../db');

function after(done){
  testUser = new db.User().where({username: 'test'}).fetch()
  .then(function(user){
    return db.Token
      .where({user_id: user.id})
      .fetchAll();
  }).then(function(tokens){
      return tokens.mapThen(function(model){ model.destroy() });
  }).then(function(){
      user.destroy();
      done();
  });
}

module.exports = {
  before: before,
  after: after,
  userData: {
      username: 'test',
      password: 'abejapass',
      email: 'test@abeja.asia',
      status: '',
      first_name: 'Abeja',
      last_name: ' User',
      role: 'admin',
      created: new Date(),
      updated: new Date(),
      token: ''
    }
}
