var assert = require('assert');
var request = require('supertest');
var app = require('../app');

var should = require('should');
var db = require('../db');
var utils = require('./util');

describe('OAuth2', function() {

  var token = "Ga22hLfp9XA6fG4YFWADmSutzpFuywrpjtfT6Ez5relMgjsK1hCTmF7fu95sJsGptyyPxz6naHSjc1IKx1IeiF6jNp9YKKavCCZCf64hGBJKMw2VAnuSkgyeXfNH2TuIvZyUPRLEtfUNlrOcix6YSHOFHP7i5OgmZQRIaa9QmiRb78S6fznOOsdM2UB24NMeWxHXBgavFreE0DimHdqLxMukPEjATuQRrgWWucUA0ZN0DdaPPWmBavK9zno3GkyY";

  var user = {
    username: 'test',
    password: 'abejapass',
    name: 'Abeja User'
  }

  before(utils.before);

  after(utils.after);

  it('should respond with an error if token is not present', function(done) {
    request(app)
      .get('/api/userinfo')
      .end(function(req, res) {
        res.status.should.equal(401);
        done();
      });
  });

  it('should grant token given username and password', function(done) {
    request(app)
      .post('/oauth/token')
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .send({
        grant_type: 'password',
        client_id: 'dashboard',
        client_secret: 'secret',
        username: user.username, // provided by the user
        password: user.password // provided by the user
      })
      .end(function(req, res) {
        res.status.should.equal(200);
        res.body.should.have.property('access_token');
        token = res.body.access_token;
        done();
      });
  });

  it('should grant access, given token', function(done) {
    request(app)
      .get('/api/userinfo')
      .set('Authorization', 'Bearer ' + token)
      .end(function(req, res) {
        res.status.should.equal(200);
        done();
      });
  });

  it('should NOT grant access with invalid token', function(done) {
    request(app)
      .get('/api/userinfo')
      .set('Authorization', 'Bearer THISISNOTAGOODTOKEN')
      .end(function(req, res) {
        res.status.should.not.equal(200);
        done();
      });
  });

});

