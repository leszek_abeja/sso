var assert = require('assert');
var request = require('supertest');
var app = require('../app');

var should = require('should');
var db = require('../db');
var utils = require('./util');

describe('api', function() {

  var testUser = db.User.where('username', 'test');

//  before(function(done){
//    return testUser.fetch().then(function(){
//      done();
//    });
//  });

  before(function(done){
    testUser = db.User
      .where(utils.userData)
      .fetch()
      .then(function(model){
        return model ? model: new db.User.create(utils.userData);
      }).then(function(){
        done();
      });
  });

  after(function(done){
    testUser.destroy().then(function(){
      done();
    });
  });

  it('should fail if user not logged in', function(done) {
    request(app)
      .get('/api/account')
      .end(function(req, res) {
        res.status.should.equal(401);
        done();
    });
  });

  it('should reject wrong password', function(done) {
    request(app)
      .post('/api/login')
      .send({
        username: 'fakeuser',
        password: 'wrongpassword'
    }).end(function(req, res) {
      res.body.should.have.property('success');
      res.body.success.should.not.be.ok;
      done();
    });
  });


  it('should let user log in', function(done) {
    request(app)
      .post('/api/login')
      .send({
        username: testUser.get('username'),
        password: testUser.get('password')
    }).end(function(req, res) {
      res.body.should.have.property('success');
      res.body.success.should.be.ok;
      done();
    });
  });

  it('should register a new user', function(done) {
    request(app)
      .post('/api/register')
      .send({
        username: 'test2',
        password: 'abejapass',
        email: 'test2@abeja.asia',
        first_name: 'Abeja',
        last_name: ' User'
    }).end(function(req, res) {
      console.log('register test');
      console.log(res.body);
      res.status.should.equal(200);
      res.body.should.have.property('success');
      res.body.success.should.be.ok;
      done();
    });
  });

});

